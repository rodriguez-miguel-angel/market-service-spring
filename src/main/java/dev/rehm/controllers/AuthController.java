package dev.rehm.controllers;

import dev.rehm.exceptions.UnauthorizedException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {

//    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestParam("username")String username,
                                        @RequestParam("password")String password){
        if(username.equals("crehm@gmail.com")){
            if(password.equals("supersecret")){
                return ResponseEntity.ok()
                        .header("Authorization", "admin-auth-token")
                        .body("success!");
            }
//            return new ResponseEntity<>("Incorrect password", HttpStatus.UNAUTHORIZED);
            throw new UnauthorizedException("Incorrect password");
        }
//        return new ResponseEntity<>("Username not recognized", HttpStatus.UNAUTHORIZED);
        throw new UnauthorizedException("Username not recognized");

    }
}
