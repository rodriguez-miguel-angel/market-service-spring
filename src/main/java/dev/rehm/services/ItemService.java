package dev.rehm.services;

import dev.rehm.models.Item;
import dev.rehm.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {

    @Autowired
    ItemRepository itemRepo;

    public List<Item> getAll(){
        return itemRepo.findAll();
    }

    public Item getById(int id){
        return itemRepo.getOne(id);
    }

    public Item create(Item item){
        return itemRepo.save(item);
    }

    public List<Item> getByName(String name){
        return itemRepo.findItemsByNameContaining(name);
    }

}
