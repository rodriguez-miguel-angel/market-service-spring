package dev.rehm.aspects;

import dev.rehm.controllers.ItemController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
public class LoggingAspect {

    private Logger logger = LogManager.getLogger(LoggingAspect.class);

    @Before("within(dev.rehm.services.*)")
    public void logMethodSignature(JoinPoint jp){
        logger.info(jp.getSignature());
    }

    @Before("within(dev.rehm.controllers.*)")
    public void logRequest(){
        HttpServletRequest request =
                ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();
        logger.info(request.getMethod() + " request made to: "+request.getRequestURI());
    }


}
