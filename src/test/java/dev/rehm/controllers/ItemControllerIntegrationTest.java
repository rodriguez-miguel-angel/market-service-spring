package dev.rehm.controllers;

import dev.rehm.models.Item;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ItemControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetOneShouldReturnCorrectItem(){
//        Item actual = this.restTemplate.getForObject("http://localhost:8082/items/2", Item.class); // this didn't
//        give us the expected result because this request was not authorized
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth-token");
        HttpEntity<Item> request = new HttpEntity<>(headers);
        ResponseEntity<Item> responseEntity = this.restTemplate.exchange("http://localhost:8082/items/2",
                HttpMethod.GET,request,
                Item.class);

        Item actual = responseEntity.getBody();
        Item expected = new Item(2, "alpine tent", 145);
        assertEquals(expected, actual);
    }

    @Test
    public void testOneUnauthorizedReturnNull(){
        Item actual = this.restTemplate.getForObject("http://localhost:8082/items/2", Item.class);
        assertNull(actual);
    }

}
