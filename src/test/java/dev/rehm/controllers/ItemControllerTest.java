package dev.rehm.controllers;

import dev.rehm.models.Item;
import dev.rehm.services.ItemService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ItemControllerTest {

    private MockMvc mockMvc;

    @MockBean
    ItemService itemService;

    @InjectMocks
    ItemController itemController;

    @BeforeEach
    public void setup(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(itemController).build();
    }

    @Test
    public void shouldReturnCorrectItems() throws Exception {
        List<Item> items = new ArrayList<>();
        items.add(new Item(1, "unicycle", 100));
        items.add(new Item(2, "3 person tent", 100));
        items.add(new Item(3, "cooking stove", 100));

        when(itemService.getAll()).thenReturn(items);

        this.mockMvc.perform(get("/items").header("Authorization", "admin-token"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].name")
                    .value(hasItems("unicycle", "3 person tent", "cooking stove")));
    }

    @Test
    public void shouldReturn401WhenNoTokenPresent() throws Exception {
        this.mockMvc.perform(get("/items"))
                .andExpect(status().isUnauthorized());

    }


}
